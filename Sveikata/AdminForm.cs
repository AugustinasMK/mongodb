﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SveikataRedis
{
    public partial class AdminForm : Form
    {

        public AdminForm(IMongoDatabase _database)
        {

            DateTime date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddTicks(-1);

            var col = _database.GetCollection<Visit>("visits");
            var aggr = col.Aggregate()
                .Match(v => v.Date >= firstDayOfMonth)
                .Match(v => v.Date <= lastDayOfMonth)
                .Group(new BsonDocument { { "_id", "$doctor" }, { "count", new BsonDocument("$sum", 1) } })
                .Sort(new BsonDocument { { "count", -1 } })
                .ToList();

            #region Strings
            var map = @"
                function(){
                    var visit = this;
                    emit(visit.doctor, { count: 1});
                }";

            string reduce = @"        
                function(key, values) {
                    var result = {count: 0};
                    values.forEach(function(value){               
                        result.count += 1;
                    });
                    return result;
            }";


            string finalize = @"
                function(key, value){
                    value.totalTime = value.count * 0.5;
                    return value;
                }";
            #endregion


            var builder = Builders<Visit>.Filter;
            var filter = builder.Gte(v => v.Date, firstDayOfMonth) & builder.Lte(v => v.Date, lastDayOfMonth);

            var options = new MapReduceOptions<Visit, BsonDocument>
            {
                OutputOptions = MapReduceOutputOptions.Inline,
                Finalize = finalize,
                Filter = filter,
            };

            var mr = col.MapReduce(map, reduce, options);
            var ml = mr.ToList().OrderByDescending(x => x[1][0]).ToList();

            InitializeComponent();

            foreach (var item in aggr)
            {
                richTextBox1.Text += "Doc code: " + item[0] + "\t Visit count: " + item[1] + "\n";
            }
            foreach (var item in ml)
            {
                richTextBox2.Text += "Doc code: " + item[0] + "\t Visit count: " + item[1][0] + "\t Hours worked: " + item[1][1] + "\n";
            }

        }
    }
}
