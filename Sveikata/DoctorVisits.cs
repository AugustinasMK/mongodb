﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class DoctorVisits : Form
    {

        private IMongoDatabase _db;
        private string _code;


        public DoctorVisits(IMongoDatabase database, string code)
        {
            _db = database;
            _code = code;
            
            InitializeComponent();

            label1.Text = "Welcome " + code;
            var worrkdays = _db.GetCollection<Doctor>("doctors").Find(d => d.Code == _code).ToList()[0].workDays;
            foreach (var wd in worrkdays)
            {
                richTextBox1.Text = $"{richTextBox1.Text}{wd.Data.Date.ToShortDateString()}\t{wd.from}\t{wd.to}\n";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WorkDay  wd = new WorkDay();
            wd.Data = dateTimePicker1.Value.Date;
            wd.@from = dateTimePicker2.Value.ToShortTimeString();
            wd.to = dateTimePicker3.Value.ToShortTimeString();


            for (DateTime d = dateTimePicker2.Value; d < dateTimePicker3.Value; d = d.AddMinutes(30))
            {
                DateTime id = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month,
                    dateTimePicker1.Value.Day, d.Hour, d.Minute, d.Second);
                AppTime at = new AppTime {time = id, taken = false};
                wd.times.Add(at);
            }

            var update = Builders<Doctor>.Update.AddToSet(d => d.workDays, wd);
            _db.GetCollection<Doctor>("doctors").UpdateOne(d => d.Code == _code, update);

            richTextBox1.Text += $@"{richTextBox1.Text}{wd.Data.Date.ToShortDateString()}	{wd.from}	{wd.to}
";


        }

        private void button3_Click(object sender, EventArgs e)
        {
            DoctorPres dp = new DoctorPres(_db, _code);
            dp.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var visits = new List<Visit>();//_db.GetCollection<Visit>("visits").Find().ToList();
            if (visits.Count == 0)
            {
                var pull = Builders<Doctor>.Update.PullFilter(x => x.workDays, Builders<WorkDay>.Filter.Eq(wd => wd.Data, dateTimePicker1.Value.Date));
                _db.GetCollection<Doctor>("doctors").FindOneAndUpdate(d => d.Code == _code, pull);
                richTextBox1.Text = "WorkDay Deleted. Please relog to see changes.";
            }
        }
    }
}
