﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class PatientsVisits : Form
    {
        //private Patient patient;
        private IMongoDatabase _db;
        private Patient P;

        private Doctor Doc;

        private List<AppTime> times = new List<AppTime>();
        private int wIndex = -1;

        public PatientsVisits(Patient p, IMongoDatabase database)
        {
            _db = database;
            P = p;

            InitializeComponent();
            label2.Text = @"Welcome " + p.Name;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string docCode = textBox1.Text.Trim();
            DateTime date = dateTimePicker1.Value.ToLocalTime().Date;


            var colection = _db.GetCollection<Doctor>("doctors");
            var builder = Builders<Doctor>.Filter;
            var filter = builder.Eq(d => d.Code, docCode);

            var col = colection.Find(filter).ToList();
            if (col.Count > 0)
            {
                Doc = col[0];
                for (int i = 0; i < Doc.workDays.Count; i++)
                {
                    if (Doc.workDays[i].Data.Date == date)
                    {
                        wIndex = i;
                        times = Doc.workDays[i].times;
                        break;
                    }
                }

                if (wIndex != -1)
                {
                    int c = 0;
                    foreach (var time in times)
                    {
                        if (!time.taken)
                        {
                            richTextBox2.Text += time.time.ToShortTimeString() + "\t";
                            c++;
                        }
                    }

                    if (c != 0)
                    {
                        button2.Visible = true;
                        dateTimePicker2.Visible = true;
                    }
                    else richTextBox2.Text = @"ERROR: Doctors que is full on this day.";
                }
                else richTextBox2.Text = @"ERROR: Doctor does not work on this day.";
            }
            else richTextBox2.Text = @"ERROR: Doctor does not exist.";



        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool valid = false;
            string timeChoice = dateTimePicker2.Value.ToShortTimeString();
            DateTime confTime = DateTime.MinValue;
            foreach (var time in times)
            {
                if (time.time.ToShortTimeString() == timeChoice)
                {
                    time.taken = true;
                    confTime = time.time;
                    break;
                } 
            }

            if (confTime != DateTime.MinValue)
            {
                Visit v = new Visit
                {
                    Date = confTime,
                    DoctorCode = Doc.Code,
                    PatientId = P.ID
                };
                _db.GetCollection<Visit>("visits").InsertOne(v);
                richTextBox2.Text = "Registration Successful";
                button2.Visible = false;
                dateTimePicker2.Visible = false;
                Doc.workDays[wIndex].times = times;

                _db.GetCollection<Doctor>("doctors").UpdateOne(d => d.ID == Doc.ID, Builders<Doctor>.Update.Set(d => d.workDays, Doc.workDays));

            }
            else
            {
                richTextBox2.Text += @"    The time you put in is not valid";
            }


        }

    }
}
