﻿using System;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class DoctorForm : Form
    {
        private IMongoDatabase _db;
        public DoctorForm(IMongoDatabase database)
        {
            _db = database;
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //RedisValue pass = _db.StringGet(textBox1.Text.Trim());

            Doctor d = new Doctor(textBox1.Text.Trim(), textBox2.Text.Trim());

            try
            {
                _db.GetCollection<Doctor>("doctors").InsertOne(d);
                DoctorVisits dv = new DoctorVisits(_db, textBox1.Text.Trim());
                dv.ShowDialog();
            }
            catch (Exception exception)
            {
                DoctorVisits dv = new DoctorVisits(_db, textBox1.Text.Trim());
                dv.ShowDialog();
            }

            //if (pass == RedisValue.Null)
            //{
            //    Console.WriteLine("User does not exist.");
            //}
            //else
            //{
            //    if (pass == textBox2.Text.Trim())
            //    {
            //        DoctorVisits d = new DoctorVisits();
            //        d.ShowDialog();
            //    }
            //    else
            //    {
            //        Debug.WriteLine("Pass not match");
            //    }
            //}
        }
    }
}
