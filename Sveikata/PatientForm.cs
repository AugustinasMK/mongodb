﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class PatientForm : Form
    {
        private IMongoDatabase _db;
        public PatientForm(IMongoDatabase database)
        {
            _db = database;
            InitializeComponent();

        }

        private void SIbut_Click(object sender, EventArgs e)
        {
            var isNumeric = long.TryParse(SIcode.Text.Trim(), out long n);
            if (isNumeric)
            {

                var colection = _db.GetCollection<Patient>("patients");

                var builder = Builders<Patient>.Filter;
                var filter = builder.Eq(p => p.Code, SIcode.Text.Trim()) & builder.Eq(p => p.Pass, SIPass.Text.Trim());

                var results = colection.Find(filter).ToList();

                if (results.Count == 0)
                {
                    MessageBox.Show(@"No such user exists.");
                }
                else if(results.Count == 1)
                {
                    SIcode.Text = "";
                    SIPass.Text = "";
                    PatientMenu pm = new PatientMenu(results[0], _db);
                    pm.ShowDialog();
                }
                else
                {
                    throw new Exception("Several accounts exist with this information despite code being uniue.");
                }
            }
            else
            {
                Debug.WriteLine("Not numeric");
            }
        }

        private void SUbut_Click(object sender, EventArgs e)
        {
            var isNumeric = long.TryParse(SUcode.Text.Trim(), out long n);
            if (isNumeric)
            {
                if (SUpass1.Text == SUpass2.Text)
                {
                    Patient p = new Patient(SUcode.Text.Trim(), SUname.Text.Trim(), SUpass1.Text.Trim(), SUmail.Text.Trim());
                    _db.GetCollection<Patient>("patients").InsertOne(p);
                    SUcode.Text = "";
                    SUname.Text = "";
                    SUpass1.Text = "";
                    SUpass2.Text = "";
                    SUmail.Text = "";
                    PatientMenu pm = new PatientMenu(p, _db);
                    pm.ShowDialog();
                }
                else Debug.WriteLine("Pass not match");
            }
            else Debug.WriteLine("Not numeric");
        }
    }
}
