﻿using System;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class PatientMenu : Form
    {
        private IMongoDatabase _db;
        private Patient P;

        public PatientMenu(Patient p, IMongoDatabase database)
        {
            _db = database;
            P = p;
            InitializeComponent();
            foreach (var prescription in P.Prescriptions)
            {
                if (prescription.ValidTrough > DateTime.Today) richTextBox1.Text += prescription.MedName + "\t" + prescription.Dose + "\t" + prescription.ValidTrough + "\n";
            }
            var builder = Builders<Visit>.Filter;
            var filter = builder.Eq(v => v.PatientId, P.ID) & builder.Gte(v => v.Date, DateTime.Today) & builder.Lte(v => v.Date, DateTime.Today.AddDays(3));
            var visits = _db.GetCollection<Visit>("visits").Find(filter).ToList();

            foreach (var visit in visits)
            {
                richTextBox2.Text += visit.Date + "\t" + visit.DoctorCode + "\t";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PatientsVisits pv = new PatientsVisits(P, _db);
            pv.ShowDialog();
        }
    }
}
