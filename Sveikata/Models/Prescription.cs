﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDB.Models
{
    public class Prescription
    {
        [BsonElement("datePre"), BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DatePrescribed { get; set; }
        [BsonElement("validTrough"), BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ValidTrough { get; set; }
        [BsonElement("medName")]
        public string MedName { get; set; }
        [BsonElement("dose")]
        public string Dose { get; set; }
        [BsonElement("docCode")]
        public string DoctorCode { get; set; }
    }
}