﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDB.Models
{
    class Doctor
    {
        public Doctor(string v1, string v2)
        {
            Code = v1;
            Pass = v2;
        }

        [BsonId]
        public ObjectId ID { get; set; }
        [BsonElement("code")]
        public string Code { get; set; }
        [BsonElement("pass")]
        public string Pass { get; set; }
        [BsonElement("workdays")]
        public List<WorkDay> workDays = new List<WorkDay>();
    }

    class WorkDay
    {
        [BsonElement("date"), BsonDateTimeOptions(Kind = DateTimeKind.Local)] public DateTime Data;
        [BsonElement("from")] public string from;
        [BsonElement("to")] public string to;
        [BsonElement("times")] public List<AppTime> times = new List<AppTime>();
    }

    class AppTime
    {
        [BsonElement("time"), BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime time;
        [BsonElement("taken")]
        public bool taken;
    }

}
