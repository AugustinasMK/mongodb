﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace MongoDB.Models
{
    [BsonIgnoreExtraElements]
    public class Patient
    {
        [BsonId]
        public ObjectId ID { get; set; }
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("code")]
        public string Code { get; set; }
        [BsonElement("pass")]
        public string Pass { get; set; }
        [BsonElement("phones")]
        public List<Phones> Phones { get; set; }
        [BsonElement("emails")]
        public List<Email> Emails { get; set; }
        [BsonElement("adresses")]
        public List<Adress> Adresses  { get; set; }
        [BsonElement("prescriptions")]
        public List<Prescription> Prescriptions { get; set; }

        public Patient(string code, string name, string pass, string email)
        {
            ID = ObjectId.GenerateNewId();
            Code = code;
            Name = name;
            Pass = pass;
            Emails = new List<Email>();
            Phones = new List<Phones>();
            Adresses = new List<Adress>();
            Prescriptions = new List<Prescription>();
            Emails.Add(new Email("default", email));
        }

    }

    public class Phones
    {
        [BsonElement("label")]
        public string Label { get; set; }
        [BsonElement("number")]
        public string Number { get; set; }
    }

    public class Adress
    {
        [BsonElement("label")]
        public string Label { get; set; }
        [BsonElement("street")]
        public string Street { get; set; }
        [BsonElement("city")]
        public string City { get; set; }
        [BsonElement("zip")]
        public string Zip { get; set; }
    }

    public class Email
    {

        public Email(string v, string email)
        {
            Label = v;
            EAdress = email;
        }

        [BsonElement("label")]
        public string Label { get; set; }
        [BsonElement("eadress")]
        public string EAdress { get; set; }
    }
}
