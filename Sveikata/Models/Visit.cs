﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDB.Models
{
    public class Visit
    {
        [BsonId]
        public ObjectId ID { get; set; }
        [BsonElement("patient")]
        public ObjectId PatientId { get; set; }
        [BsonElement("doctor")]
        public string DoctorCode { get; set; }
        [BsonElement("time"), BsonDateTimeOptions(Kind = DateTimeKind.Local)] public DateTime Date;

    }
}