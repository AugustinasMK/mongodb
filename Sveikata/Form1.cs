﻿using System;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Models;


namespace SveikataRedis
{
    public partial class Form1 : Form
    {

        public void ClearDb()
        {
            //var client = new MongoClient("mongodb+srv://augment:Alvydas1@mifas-xosx2.mongodb.net/test?retryWrites=true");
            var client = new MongoClient("mongodb://localhost:27017");
            client.DropDatabase("sveikata");
            _database = client.GetDatabase("sveikata");
            _database.CreateCollection("patients");
            _database.CreateCollection("doctors");
            _database.CreateCollection("visits");

            var options = new CreateIndexOptions() { Unique = true };

            var field = new StringFieldDefinition<Patient>("code");
            var indexDefinition = new IndexKeysDefinitionBuilder<Patient>().Ascending(field);
#pragma warning disable CS0618 // Type or member is obsolete
            _database.GetCollection<Patient>("patients").Indexes?.CreateOne(indexDefinition, options);
#pragma warning restore CS0618 // Type or member is obsolete

            var fieldD = new StringFieldDefinition<Doctor>("code");
            var indexDefinitionD = new IndexKeysDefinitionBuilder<Doctor>().Ascending(fieldD);
#pragma warning disable CS0618 // Type or member is obsolete
            _database.GetCollection<Doctor>("doctors").Indexes?.CreateOne(indexDefinitionD, options);
#pragma warning restore CS0618 // Type or member is obsolete


            var fieldV1 = new StringFieldDefinition<Visit>("doctor");
            var fieldV2 = new StringFieldDefinition<Visit>("time");
            var indexDefV = new IndexKeysDefinitionBuilder<Visit>().Ascending(fieldV1).Ascending(fieldV2);
#pragma warning disable CS0618 // Type or member is obsolete
            _database.GetCollection<Visit>("visits").Indexes?.CreateOne(indexDefV, options);
#pragma warning restore CS0618 // Type or member is obsolete




        }

        private IMongoDatabase _database;

        public Form1()
        {
            
            var client = new MongoClient("mongodb://localhost:27017");
            _database = client.GetDatabase("sveikata");
            //ClearDb();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PatientForm p = new PatientForm(_database);
            p.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DoctorForm d = new DoctorForm(_database);
            d.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button4.Visible = true;
            textBox1.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "gaja")
            {
                button4.Visible = false;
                textBox1.Text = "";
                textBox1.Visible = false;
                AdminForm a = new AdminForm(_database);
                a.ShowDialog();
            }
        }
    }
}
