﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Models;

namespace SveikataRedis
{
    public partial class DoctorPres : Form
    {

        private IMongoDatabase _db;
        private string _code;

        public DoctorPres(IMongoDatabase database, string code)
        {
            _db = database;
            _code = code;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Prescription prescription = new Prescription();
            prescription.MedName = textBox2.Text.Trim();
            prescription.DoctorCode = _code;
            prescription.Dose = textBox3.Text.Trim();
            prescription.ValidTrough = dateTimePicker1.Value.Date;
            prescription.DatePrescribed = DateTime.Today;
            var update = Builders<Patient>.Update.AddToSet(p => p.Prescriptions, prescription);
            _db.GetCollection<Patient>("patients").FindOneAndUpdate(p => p.Code == textBox1.Text.Trim(), update);
        }
    }
}
