﻿namespace SveikataRedis
{
    partial class PatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SIPass = new System.Windows.Forms.TextBox();
            this.SUname = new System.Windows.Forms.TextBox();
            this.SUmail = new System.Windows.Forms.TextBox();
            this.SUpass1 = new System.Windows.Forms.TextBox();
            this.SUpass2 = new System.Windows.Forms.TextBox();
            this.SIbut = new System.Windows.Forms.Button();
            this.SUbut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SUcode = new System.Windows.Forms.TextBox();
            this.SIcode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // SIPass
            // 
            this.SIPass.Location = new System.Drawing.Point(86, 139);
            this.SIPass.Name = "SIPass";
            this.SIPass.PasswordChar = '*';
            this.SIPass.Size = new System.Drawing.Size(202, 22);
            this.SIPass.TabIndex = 1;
            // 
            // SUname
            // 
            this.SUname.Location = new System.Drawing.Point(522, 139);
            this.SUname.Name = "SUname";
            this.SUname.Size = new System.Drawing.Size(211, 22);
            this.SUname.TabIndex = 3;
            // 
            // SUmail
            // 
            this.SUmail.Location = new System.Drawing.Point(522, 198);
            this.SUmail.Name = "SUmail";
            this.SUmail.Size = new System.Drawing.Size(211, 22);
            this.SUmail.TabIndex = 4;
            // 
            // SUpass1
            // 
            this.SUpass1.Location = new System.Drawing.Point(522, 252);
            this.SUpass1.Name = "SUpass1";
            this.SUpass1.PasswordChar = '*';
            this.SUpass1.Size = new System.Drawing.Size(211, 22);
            this.SUpass1.TabIndex = 5;
            // 
            // SUpass2
            // 
            this.SUpass2.Location = new System.Drawing.Point(522, 313);
            this.SUpass2.Name = "SUpass2";
            this.SUpass2.PasswordChar = '*';
            this.SUpass2.Size = new System.Drawing.Size(211, 22);
            this.SUpass2.TabIndex = 6;
            // 
            // SIbut
            // 
            this.SIbut.Location = new System.Drawing.Point(86, 202);
            this.SIbut.Name = "SIbut";
            this.SIbut.Size = new System.Drawing.Size(202, 23);
            this.SIbut.TabIndex = 7;
            this.SIbut.Text = "Sign In";
            this.SIbut.UseVisualStyleBackColor = true;
            this.SIbut.Click += new System.EventHandler(this.SIbut_Click);
            // 
            // SUbut
            // 
            this.SUbut.Location = new System.Drawing.Point(522, 359);
            this.SUbut.Name = "SUbut";
            this.SUbut.Size = new System.Drawing.Size(211, 23);
            this.SUbut.TabIndex = 8;
            this.SUbut.Text = "Sign Up";
            this.SUbut.UseVisualStyleBackColor = true;
            this.SUbut.Click += new System.EventHandler(this.SUbut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Asmens Kodas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Slaptažodis";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(519, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Asmens Kodas";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(519, 119);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(110, 17);
            this.label.TabIndex = 12;
            this.label.Text = "Vardas Pavardė";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(519, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "El. pašto adresas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(519, 232);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Slaptažodis";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(519, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Pakartokite slaptažodį";
            // 
            // SUcode
            // 
            this.SUcode.Location = new System.Drawing.Point(522, 90);
            this.SUcode.Name = "SUcode";
            this.SUcode.Size = new System.Drawing.Size(211, 22);
            this.SUcode.TabIndex = 2;
            // 
            // SIcode
            // 
            this.SIcode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.SIcode.Location = new System.Drawing.Point(86, 90);
            this.SIcode.Name = "SIcode";
            this.SIcode.Size = new System.Drawing.Size(202, 22);
            this.SIcode.TabIndex = 0;
            // 
            // PatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SUbut);
            this.Controls.Add(this.SIbut);
            this.Controls.Add(this.SUpass2);
            this.Controls.Add(this.SUpass1);
            this.Controls.Add(this.SUmail);
            this.Controls.Add(this.SUname);
            this.Controls.Add(this.SUcode);
            this.Controls.Add(this.SIPass);
            this.Controls.Add(this.SIcode);
            this.Name = "PatientForm";
            this.Text = "PatientForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox SIPass;
        private System.Windows.Forms.TextBox SUname;
        private System.Windows.Forms.TextBox SUmail;
        private System.Windows.Forms.TextBox SUpass1;
        private System.Windows.Forms.TextBox SUpass2;
        private System.Windows.Forms.Button SIbut;
        private System.Windows.Forms.Button SUbut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SUcode;
        private System.Windows.Forms.TextBox SIcode;
    }
}